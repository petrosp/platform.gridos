#!/usr/bin/env bash

_custom_init() {
  local crm=""
  if [[ ${DOCKMAN_NO_INIT_SCRIPTS} -eq 1 ]]; then
    crm="$crm --skip-startup-files"
    # /sbin/my_init --skip-startup-files -- "$@"
  fi
  if [[ ${DOCKMAN_QUIET} -eq 1 ]]; then
    crm="$crm --quiet"
    # /sbin/my_init --quiet -- "$@"
  fi
    /sbin/my_init $crm -- "$@"
}

_post_init() {

  if [[ ${DOCKMAN_DEBUG} -eq 1 ]]; then
    _custom_init "$@"
  else
    _custom_init /sbin/setuser app "$@"
  fi
}

_post_init "$@"
