####### base STAGE ======================
FROM amsix/s2i-ruby25-ubi8:latest

ARG LOCAL_GROUP_ID
ENV CONT_USER_GID=${LOCAL_GROUP_ID:-0} \
    GEM_HOME=/opt/app-root/vendor/bundle/ruby/2.6.0

LABEL maintainer="PetrosP <petros@ams-ix.net>"

ENV ROR_BUILDER_VERSION 6.0

LABEL io.k8s.description="Platform for building ruby 2.6 and rails 6.0.x apps" \
  io.k8s.display-name="RoR builder 6.0.x" \
  io.openshift.expose-services="8080:http" \
  io.openshift.tags="builder,rails,ror,ruby,s2i,2.6,6.0.x" \
  io.openshift.s2i.scripts-url="image:///usr/libexec/s2i"

USER root

WORKDIR /opt/app-root/src/webapp
COPY .s2i/Gemfiles.r6/Gemfile /opt/app-root/src/webapp/Gemfile
RUN mkdir -p tmp/cache \
        tmp/pids \
        tmp/sockets \
        tmp/var/run \
        log \
  && chown -R 1001:${CONT_USER_GID} /opt/app-root/src/webapp

USER 1001

RUN bundle config set --global path /opt/app-root/vendor/bundle \
  && bundle config set --global jobs 8 \
  && bundle config set --global retry 3 \
  && bundle config set --global build.pg "--with-pg-config=/usr/bin/pg_config" \
  && bundle install

USER root

RUN gem clean all && \
    dnf clean all -y && \
    rm -rf /var/cache/yum

# # Copy useful scripts
# COPY s2i/generate-container-user /opt/app-root/etc/generate-container-user
# RUN chmod a+x /opt/app-root/etc/generate-container-user

# Make the content of /opt/app-root owned by user 1001


RUN rm -rf /opt/app-root/src/webapp/public
RUN chown -R 1001:${CONT_USER_GID} ${APP_ROOT}
COPY .s2i/fix-permissions /usr/bin/fix-permissions

USER 1001

EXPOSE 8080

WORKDIR /opt/app-root/src/webapp
