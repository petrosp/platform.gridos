# User specific aliases and functions
echo 'home/.bash_aliases'

alias bor='bin/rails'
alias borc='bor c'
alias borg='bor g'
alias borx='bor d'
alias bordm='bor db:migrate'
alias bb='bin/bundle'
alias bbe='bin/bundle exec'
