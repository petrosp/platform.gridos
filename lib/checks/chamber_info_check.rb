# Exctract information about git branch, commit SHA
class ChamberInfoCheck < OkComputer::Check
  def check
    if chamber_env
      mark_message chamber_env
    else
      mark_failure
      mark_message 'There is no info available'
    end
  end

  private

  def chamber_env
    Chamber.settings.namespaces.raw_namespaces
  end

end

