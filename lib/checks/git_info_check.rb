# Exctract information about git branch, commit SHA
class GitInfoCheck < OkComputer::Check
  def check
    if git_env
      mark_message git_env
    else
      mark_failure
      mark_message 'There is no info available'
    end
  end

  private

  def custom_git_env
    ENV.fetch('GIT_DESC', nil)
  end

  def openshift_git_env
    commit = ENV.fetch('OPENSHIFT_BUILD_COMMIT', nil)
    return false if commit.nil?

    commit[0...10]
  end

  def file_based_git_env
    asset = Pathname.new(Rails.root).join('GITINFO')
    return nil unless asset.exist?

    content = File.read(asset)
    content.strip
  end

  def git_env
    file_based_git_env || custom_git_env || openshift_git_env
  end
end

