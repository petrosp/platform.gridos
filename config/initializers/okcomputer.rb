require_relative '../../lib/checks/git_info_check'
require_relative '../../lib/checks/chamber_info_check'

OkComputer.mount_at = 'status'
# OkComputer::Registry.register 'database', OkComputer::ActiveRecordCheck.new
OkComputer::Registry.register 'ruby-version', OkComputer::RubyVersionCheck.new
OkComputer::Registry.register 'git-info', GitInfoCheck.new
OkComputer::Registry.register 'chamber-info', ChamberInfoCheck.new
